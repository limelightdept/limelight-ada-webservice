<?php
use Limelight\Plugins\ADA_Webservice\Webservice;
use Limelight\Plugins\ADA_Webservice\Activator;
use Limelight\Plugins\ADA_Webservice\Deactivator;
use Limelight\Plugins\ADA_Webservice\API;
use Limelight\Plugins\ADA_Webservice\i18n;
use Limelight\Plugins\ADA_Webservice\Loader;

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://limelightdept.com
 * @since             1.0.0
 * @package           Limelight_Ada_Webservice
 *
 * @wordpress-plugin
 * Plugin Name:       Limelight ADA Webservice
 * Plugin URI:        https://limelightdept.com/
 * Description:       Scans a web domain for WCAG 2.0 AA Compliance and returns results
 * Version:           1.0.0
 * Author:            Limelight Dept
 * Author URI:        https://limelightdept.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       limelight_ada_webservice
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WEBSERVICE_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-limelight_ada_webservice-activator.php
 */
function activate_limelight_ada_webservice() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-limelight_ada_webservice-activator.php';
	Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-limelight_ada_webservice-deactivator.php
 */
function deactivate_limelight_ada_webservice() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-limelight_ada_webservice-deactivator.php';
	Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_limelight_ada_webservice' );
register_deactivation_hook( __FILE__, 'deactivate_limelight_ada_webservice' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-limelight_ada_webservice.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_limelight_ada_webservice() {

	$plugin = new Webservice();
	$plugin->run();

}
run_limelight_ada_webservice();
