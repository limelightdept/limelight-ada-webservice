<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Limelight_Ada_Webservice
 * @subpackage Limelight_Ada_Webservice/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
