<?php
namespace Limelight\Plugins\ADA_Webservice;
/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Limelight_Ada_Webservice
 * @subpackage Limelight_Ada_Webservice/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Limelight_Ada_Webservice
 * @subpackage Limelight_Ada_Webservice/includes
 * @author     Your Name <email@example.com>
 */
class Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
