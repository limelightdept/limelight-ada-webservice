<?php
namespace Limelight\Plugins\ADA_Webservice;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

/**
*
*/

class API {
    private static $sitemap;
    private static $errors;
    private static $return_content;
    private static $client;
    private static $crawler;

    public static function init(){
        add_action( "wp_enqueue_scripts", [ __CLASS__, "enqueue_scan_script"] );

        self::$sitemap = [ "domain" => "", "non_query_links" => [], "query_links" => [] ];
        self::$return_content  = [];
        self::$errors  = [];
        self::$client  = new Client();
    }

	public static function get_sitemap(){
		return self::$sitemap;
	}

    public static function get_errors(){
        return self::$errors;
    }

    function enqueue_scan_script(){
        $jsFile = dirname(__DIR__)."/public/js/limelight_ada_webservice-public.js";
        wp_register_script( "compliance-audit-script", $jsFile, array("jquery"), "1.0", true );
        wp_localize_script( "compliance-audit-script", "ajax", array("url" => admin_url( "admin-ajax.php" )) );
        wp_enqueue_script( "compliance-audit-script" );
    }

    public static function create_sitemap($domain, $user_id){
		self::$sitemap["domain"] = $domain;
        return self::parse_public_pages();
    }

    private static function get_sitemap_from_file($path, $use_query_strings){
        $row = 0;
        $headers = [ "query_links", "non_query_links" ];
    	if (($handle = fopen( $path, "r" )) !== FALSE) {
            $key = "";
    		while (($data = fgetcsv( $handle, 0, "," )) !== FALSE) {
    			$data = array_shift( $data );
                if(in_array( $data, $headers )){
    				$key = $data;
    				continue;
    			}
    			if( $key === "query_links" AND !$use_query_strings ){
    				continue;
    			}
    			self::$sitemap["pages"][] = $data;
    			$row++;
    		}
    		fclose( $handle );
    	}
    }

    private static function get_anchor_tags(){
        self::$crawler->filter("a")->each(function ($node){
            foreach($node AS $a){
                //Building it this way gives us the html from the start of the element to the end of the element
                self::$return_content[] = $a->ownerDocument->saveHTML($a);
            }
        });
    }

    private static function parse_public_pages(){
		$domain = strtolower(self::$sitemap["domain"]);

		self::$crawler = self::$client->request('GET', "http://".$domain);
        self::get_anchor_tags();
		
        foreach( self::$return_content AS $a ){
			//Strip text down to protocol, : example href="http//www.domain.com/contact
            preg_match( '/href="[^"]*"/', $a, $matches );
			
			if(!empty($matches)){
				$a = $matches[ 0 ];			
				$a = substr( $a, 5 );
				self::parse_url($a);
			}
        }
		
        return self::$sitemap;
		
    }

    public static function add_to_errors( $e, $extra_data = [] ){
        if( !empty(self::$errors) ){
            foreach( self::$errors AS $error => $data ){
                if($e !== $error AND empty($extra_data)){
                    self::$errors[] = $e;
                    return;
                }

                if($e === $error){
                    if( !in_array($extra_data, $data )){
                        self::$errors[$error][] = $extra_data;
                        return;
                    }
                    return;
                }
            }
        }

        if(!empty($extra_data)){
            self::$errors[$e][] = $extra_data;
            return;
        }

        self::$errors[] = $e;
    }

	public static function parse_url( $url, $registered_domain = "" ){
        if( empty($registered_domain) ){ $registered_domain = self::$sitemap["domain"]; }
        if( !empty($registered_domain) ){ self::$sitemap["domain"] = $registered_domain; }
		
		//Strip the protocol from the text and remove extra quotes
		//Example: domain.com/contact
		$protocol_regex = '/(https?:\/\/)*(www.)?/';
		$url = trim(preg_replace($protocol_regex, "", $url), '"');
		
		//Filter for same domain only
        $url_domain = explode("/", $url)[0];
		if($url_domain !== $registered_domain AND $url !== "/" AND !empty($url_domain)){
			self::add_to_errors("domain mismatch", ["wrong_domain" => $url_domain, "registered_domain" => $registered_domain]);
            return;
		}
		
		//Strip off domain
		$url = preg_replace('/'.$registered_domain.'/', "", $url);
        if($url !== "/"){ $url = untrailingslashit( $url ); }
		
        self::add_to_sitemap($url);
	}

	public function add_to_sitemap($url){
		if(strpos($url, "?") !== FALSE){
			if(!in_array( $url, self::$sitemap["query_links"] )){ self::$sitemap["query_links"][] = $url; }
		}else{
			if(!in_array( $url, self::$sitemap["non_query_links"] )){ self::$sitemap["non_query_links"][] = $url; }
		}
	}
	
}
