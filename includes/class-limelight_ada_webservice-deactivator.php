<?php
namespace Limelight\Plugins\ADA_Webservice;
/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Limelight_Ada_Webservice
 * @subpackage Limelight_Ada_Webservice/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Limelight_Ada_Webservice
 * @subpackage Limelight_Ada_Webservice/includes
 * @author     Your Name <email@example.com>
 */
class Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
